package demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

	final Logger logger = LoggerFactory.getLogger(DemoController.class);

	@Value("${app.name}")
	private String name;

	@RequestMapping("/")
	String home() {
		logger.info("mon info {}, {}", name, 2);
		logger.debug("mon debug {}, {}", name, 3);
		return "hello " + name;
	}
}
